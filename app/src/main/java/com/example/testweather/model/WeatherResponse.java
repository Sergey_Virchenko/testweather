
package com.example.testweather.model;

import com.google.gson.annotations.SerializedName;

public class WeatherResponse {
    @SerializedName("query")
    public Query query;

    public Integer getTemperature() {
        if (!hasResults()) {
            return null;
        }
        return query.getResults().getChannel().getItem().getCondition().getTempInC();
    }

    public boolean hasResults() {
        return query.getResults() != null;
    }

    public String getWeather() {
        if (!hasResults()) {
            return null;
        }
        return query.getResults().getChannel().getItem().getCondition().getWeather();
    }
}
