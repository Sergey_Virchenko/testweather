
package com.example.testweather.model;

import com.google.gson.annotations.SerializedName;

public class Query {
    @SerializedName("results")
    private Results results;

    public Results getResults() {
        return results;
    }
}
