
package com.example.testweather.model;

import com.google.gson.annotations.SerializedName;

public class Item {
    @SerializedName("condition")
    private Condition condition;

    public Condition getCondition() {
        return condition;
    }
}
