
package com.example.testweather.model;

import com.google.gson.annotations.SerializedName;

public class Condition {

    @SerializedName("temp")
    private int temp;
    @SerializedName("text")
    private String text;


    public int getTempInC() {
        return (int) ((temp - 32) * (5.0 / 9.0));
    }

    public String getWeather() {
        return text;
    }
}
