
package com.example.testweather.model;

import com.google.gson.annotations.SerializedName;

public class Channel {
    @SerializedName("item")
    private Item item;

    public Item getItem() {
        return item;
    }
}
