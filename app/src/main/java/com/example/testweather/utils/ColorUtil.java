package com.example.testweather.utils;

import android.content.Context;
import android.graphics.Color;

/**
 * Created by sergey on 07.06.16.
 */
public class ColorUtil {

    private static int LOWEST_ABS = 80;//somewhat rounded lowest temperature on earth, finded in internet
    private static int HIGHEST_ABS = 50;//same
    private static int MAX_ALPHA = 255;

    private static int SCALE_WARM = MAX_ALPHA / HIGHEST_ABS;
    private static int SCALE_COLD = MAX_ALPHA / LOWEST_ABS;

    public static int getColor(Integer temperature, Context context) {
        if (temperature == null || temperature == 0) {
            return context.getResources().getColor(android.R.color.white);
        }

        if (temperature > 0) {
            return Color.argb(temperature * SCALE_WARM, 255, 0, 0);
        } else {
            return Color.argb(temperature * SCALE_COLD, 0, 0, 255);
        }
    }

}
