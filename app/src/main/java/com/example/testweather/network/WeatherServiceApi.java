package com.example.testweather.network;

import com.example.testweather.model.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by sergey on 07.06.16.
 */
public interface WeatherServiceApi {
    @GET
    Call<WeatherResponse> getWeather(@Url String url);
}
