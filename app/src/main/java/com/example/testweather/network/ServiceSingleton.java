package com.example.testweather.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sergey on 07.06.16.
 */
public class ServiceSingleton {

    private static ServiceSingleton instance;
    private WeatherServiceApi weatherService;

    private ServiceSingleton() {
    }

    public static ServiceSingleton getInstance() {
        if (instance == null) {
            instance = new ServiceSingleton();
        }
        return instance;
    }

    private void initService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://developer.yahoo.com/weather/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        weatherService = retrofit.create(WeatherServiceApi.class);
    }

    public WeatherServiceApi getService() {
        if (weatherService == null) {
            initService();
        }


        return weatherService;
    }

    public static String getWeatherUrl(double lat, double lon) {
        return "https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places%20where%20text%3D%22(" + lat + "," + lon + ")%22%20limit%201)&format=json";
    }
}
